package ca.ubc.ece.eece210.mp2;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class GenreTest {

	@Test
	public void testToString(){
		Genre rock = new Genre("rock");
		Genre alt = new Genre("alternative");
		Album arctic = new Album("Humbug//Arctic Monkeys//Crying Lightning//alternative");
		Album boc = new Album("Don't Fear the Reaper//Blue Oyster Cult//Astronomy//rock");
		
		boc.addToGenre(rock);
		rock.addToGenre(alt);
		arctic.addToGenre(alt);
		
		String stringRep = "#rock#punk#grunge@Nevermind//Nirvana//Breed//$grunge;%@American Idiot//Green Day//Whatsername//$punk;%#alternative@Humbug//Arctic Monkeys//Crying Lightning//$alternative;@Indie 2//Artist//Songs!//$alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$rock;@Destroyer//Kiss//Detroit Rock City//$rock;%";
		Genre restored = Genre.restoreCollection(stringRep);
		
		System.out.println(stringRep);
		System.out.println(restored.toString());
		
		assertTrue(restored.toString().equals(stringRep));
	}
	
}
