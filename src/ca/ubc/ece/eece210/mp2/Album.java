package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 * This class contains the information needed to represent 
 * an album in our application.
 * 
 */

public final class Album extends Element {
	 
	private char delim = '$';
	private String title, performer;
	private ArrayList<String> songList;
	private Genre genre;

	/**
	 * Builds an album with the given title, performer and song list
	 * 
	 * @param title
	 *            the title of the album
	 * @param author
	 *            the performer 
	 * @param songlist
	 * 			  the list of songs in the album
	 */
	public Album(String title, String performer, ArrayList<String> songlist) {
		genres = null; //Initializes Element with null children branches
		albums = null; 
		
		this.title = title;
		this.performer = performer;
		songList = new ArrayList<String>();
		
		for(String s : songlist){ //adds elements of param songlist to global songList
			songList.add(s);
		}
		
		if(!Genre.genreMap.containsKey("unclassified"))
			Genre.genreMap.put("unclassified", new Genre("unclassified"));
		
		addToGenre(Genre.genreMap.get("unclassified"));
	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * 
	 * @param stringRepresentation
	 *            the string representation in the form "title//performer//song1//song2//...//songN//$genre//"
	 */
	public Album(String stringRepresentation) {
		genres = null; //Initializes Element with null children branches
		albums = null; 
			
		String[] repres = stringRepresentation.split("//"); //Array of title, performer, and songs
		
		//Building Album
		title = repres[0];
		performer = repres[1];
		
		songList = new ArrayList<String>();
		int x, hasGenre;
		
		if(repres[repres.length - 1].charAt(0) == delim) //
			hasGenre = 1;								 //
		else											 // If representation has a genre tag:
			hasGenre = 0;								 //    add all but last item of repres to songList
														 // If representation has no genre tag:
		for(x = 2; x < repres.length - hasGenre; x++)	 //    add all items of repres to songList
			songList.add(repres[x]);					 //
		
		//Adding Album to Genre
		String key;
		if(x == repres.length) //if no Genre tag (#)
			key = "unclassified";
		else
			key = repres[x].substring(1).toLowerCase();
			
		if(Genre.genreMap.containsKey(key)){
			this.addToGenre(Genre.genreMap.get(key));
		}
		else{
			Genre g = new Genre(key);
			Genre.genreMap.put(key, g);
			this.addToGenre(g);
		}
	}

	/**
	 * Returns the string representation of the given album. The representation
	 * contains the title, performer and songlist, as well as all the genre
	 * that the book belongs to.
	 * 
	 * @return the string representation where title, performer, each song, and genre are 
	 * 									 separated by "//", genre is preceded by "$"
	 */
	public String toString() {
		
		String album = title + "//" + performer;
	
		for(int index = 0; index < songList.size(); index++){
			album += "//" + songList.get(index);
		}
		
		album += "//" + delim + genre.name;
		
		return album;
	}

	/**
	 * Add the Album to the given genre
	 * 
	 * @param genre
	 *            the genre to add the album to.
	 */
	public void addToGenre(Genre genre) {	
		genre.addToGenre(this); //adds this node to the genre
		this.genre = genre;     //creates reference to the genre
	}
	/**
	 * Removes and album from its genre and adds it to unclassified
	 * Effects: if unclassified does not exists, creates Genre unclassified
	 */
	public void removeFromGenre(){
		genre.albums.remove(this);
		
		if(!Genre.genreMap.containsKey("unclassified"))
			new Genre("unclassified");
		
		this.addToGenre(Genre.genreMap.get("unclassified"));
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to
	 */
	public Genre getGenre() {
		return genre;
	}

	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return the performer
	 */
	public String getPerformer() {
		return performer;
	}

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}

	/**
	 * Returns the list of songs of the album
	 * 
	 * @return the list of songs
	 */
	public ArrayList<String> getSongList() {
		return songList;
	}
}
