package ca.ubc.ece.eece210.mp2;

import static org.junit.Assert.*;

import org.junit.Test;

public class CatalogueTest {

	@Test
	public void TestToString(){
		Catalogue c = new Catalogue("testCatalogue.txt");
		//Album a = new Album("Nevermind//Nirvana//Breed//#Grunge");
		//Album b = new Album("Nevermind//Nirvana//Breed//Grunge");
		
		//a = b;
		
		String stringRep = "#Rock#Punk#Grunge@Nevermind//Nirvana//Breed//$Grunge;%@American Idiot//Green Day//Whatsername//$Punk;%#Alternative@Humbug//Arctic Monkeys//Crying Lightning//$Alternative;@Indie 2//Artist//Songs!//$Alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$Rock;@Destroyer//Kiss//Detroit Rock City//$Rock;%@Saturday Night Fever//John Travolta//Saturday Night Fever//$Disco;#Jazz#Blues@Blues Album//Armstrong//Song1//Song2//$Blues;%@Best Of//Chet Baker//My Funny Valentine//$Jazz;%@Monstercat 008//Monstercat//Song//Song2//Song3;";
		String stringRepOut = "#Rock#Punk#Grunge@Nevermind//Nirvana//Breed//$Grunge;%@American Idiot//Green Day//Whatsername//$Punk;%#Alternative@Humbug//Arctic Monkeys//Crying Lightning//$Alternative;@Indie 2//Artist//Songs!//$Alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$Rock;@Destroyer//Kiss//Detroit Rock City//$Rock;%#Jazz#Blues@Blues Album//Armstrong//Song1//Song2//$Blues;%@Best Of//Chet Baker//My Funny Valentine//$Jazz;%#unclassified@Saturday Night Fever//John Travolta//Saturday Night Fever//$Disco;@Monstercat 008//Monstercat//Song//Song2//Song3;%";		
		System.out.println(stringRepOut);
		System.out.println(c.toString());
		
		assertTrue(c.toString().equals(stringRepOut));
	}
	
}
