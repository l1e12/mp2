package ca.ubc.ece.eece210.mp2;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class AlbumTest {

	@Test
	public void testToString(){
		String title = "Appeal to Reason";
		String performer = "Rise Against";
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Collapse");
		songlist.add("Long Forgotten Sons");
		songlist.add("Re-Education (Through Labour)");

		String repres = "Appeal to Reason//Rise Against//Collapse//Long Forgotten Sons//Re-Education (Through Labour)";
		
		Album a = new Album(title, performer, songlist);
		
		assertTrue(repres.equals(a.toString()));
		
		Album b = new Album(repres);
	
		assertTrue(a.getTitle().equals(b.getTitle()));
		assertTrue(a.getPerformer().equals(b.getPerformer()));
		for(int x = 0; x < a.getSongList().size(); x++){
			assertTrue(a.getSongList().get(x).equals(b.getSongList().get(x)));
		}
	}

/*
	
	@Test
	public void test() {
		
		String title = "DiscoTunez";
		String performer = "FunkMaster6";
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Boptastic");
		songlist.add("Dem Platform Shoez");
		songlist.add("Dont stop the Disco ball");
		
		Album DiscoTunez = new Album(title,performer,songlist);
		
		assertTrue(DiscoTunez.title.equals("DiscoTunez"));
		assertTrue(DiscoTunez.performer.equals("FunkMaster6"));
		String song1 = DiscoTunez.songList.get(0);
		String song3 = DiscoTunez.songList.get(2);
		assert(song1 == "Boptastic");
		assert(song3 == "Dont stop the Disco ball");
	}
	
	@Test
	public void test2(){
		
		String title = "DiscoTunez";
		String performer = "FunkMaster6";
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Boptastic");
		songlist.add("Dem Platform Shoez");
		songlist.add("Dont stop the Disco ball");
		
		Album DiscoTunez = new Album(title,performer,songlist);
		
		//test get title
		String title1 = DiscoTunez.getTitle();
		assertTrue(title1.equals(title));
		
		//test get performer
		String per = DiscoTunez.getTitle();
		assertTrue(per.equals(title));
		
		String album = DiscoTunez.toString();
		System.out.println( album);
		
		
		String gen1 = "Funk";
		String gen2 = "Disco";
		Genre disco = new Genre(gen2);
		Genre funk = new Genre(gen1);
		
		funk.addToGenre(disco);
		DiscoTunez.addToGenre(disco);
		
		System.out.println(disco.name);
		System.out.println(disco.elements.get(0));
	}
*/

}
