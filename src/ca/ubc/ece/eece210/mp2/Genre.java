package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Genre extends Element {
	public static Map<String, Genre> genreMap = new HashMap<String, Genre>();
	public static List<String> subGenres = new ArrayList<String>();
	
	public String name;
	
    public static final String delims = "#@%;"; //string of delimeters to be used in writing/reading String representations
	
	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name  the name of the genre.
	 */
	public Genre(String name) {
		genres = new ArrayList<Genre>();
		albums = new ArrayList<Album>();//creates empty lists of genres and albums
		this.name = name;
		genreMap.put(name, this);
	}

	/**
	 * Restores a genre from its given string representation.
	 * 
	 * @param stringRepresentation where # indicates the start of a genre
	 * 									 @ indicates the start of an album
	 * 									 ; ends albums
	 * 									 % ends genres
	 * 									 and albums are in their String representation form
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		Genre restored;
		String temp = "";
		int index = 1;
		
		temp = stringBefore(stringRepresentation.substring(index));
		index += temp.length();
		
		restored = new Genre(temp); //Creates named genre from representation
		genreMap.put(temp.toLowerCase(), restored);
	
		while(index < stringRepresentation.length()){//for(int x = 0; x < counter; x++){
			char cIndex = stringRepresentation.charAt(index);

			if(cIndex == delims.charAt(0)){ //hits "#"
				Genre g = Genre.restoreCollection(stringRepresentation.substring(index));
				restored.addToGenre(g);
				subGenres.add(g.name);
				index += g.toString().length();
			}
			else if(cIndex == delims.charAt(1)){ //hits "@"
				temp = stringBefore(stringRepresentation.substring(index + 1));
				new Album(temp);
				index += temp.length() + 1;
			}
			else if(cIndex == delims.charAt(2)) //hits "%"
				return restored;
			else //hits ";"
				index++;
		}
		
		return restored;
	}
	
	/**
	 * A helper function to restoreCollection. Finds the strings between special characters held in delims
	 * 
	 * @param rep  portion of a stringRepresentation of a genre
	 * 
	 * @return the first string within rep before a character contained in delim
	 */
	public static String stringBefore(String rep){
		String before = "";
		
		int index = 0;
		while (!delims.contains("" + rep.charAt(index))){ //while the current character is not a delimeter
			before += rep.charAt(index);
			index++;
		}
		
		return before;
	}

	/**
	 * Returns the string representation of a genre
	 * 
	 * @return string representation of genre where # indicates the start of a genre
	 * 												@ indicates the start of an album
	 * 												; ends albums
	 * 												% ends genres (and by extension albums)
	 * 												and albums are in their String representation form
	 * eg. the tree:
	 *                 rock
	 *    alternative         Don't Fear the Reaper
	 *  ____      Humbug      Destroyer
	 *          
	 *  is represented as: 
	 *  #rock#alternative@Humbug.toString()%@Don't Fear the Reaper.toSting();;Destroyer.toString%
 	 */
	public String toString() {
		String repres = "#" + name;
		
		for(Element e : genres) //add string representation of each subgenre to repres
			repres += e.toString();
		
		for(Element e : albums) //add string representation of each 
			repres += "@" + e.toString() + ";";
		
		repres += "%"; //ends this genre
		
		return repres;
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @param b  the element to be added to the collection.
	 */
	public void addToGenre(Element b) {
		addChild(b);
	}

	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
	
}