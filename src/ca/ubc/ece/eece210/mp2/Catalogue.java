package ca.ubc.ece.eece210.mp2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {
     ArrayList<Genre> music;
     FileInputStream fileReader;
     PrintWriter fileWriter;
     BufferedReader musicReader;
    
     
	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		music = new ArrayList<Genre>();
	}

	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		music = new ArrayList<Genre>();
		//Try to read the file and report an error if this is not possible.	
		try {
			fileReader = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		
		String allData;
	    try {
            BufferedReader musicReader = new BufferedReader(
            		new InputStreamReader(fileReader));
            //Save all of the data to a string.
            allData = musicReader.readLine();
            
	    }catch (Exception e) {
           //Throw an exception if an error occurred during the buffered reading
            throw new RuntimeException(e);
        }
	    //Using the format that we have made to separate genres separate super-genres 
	    //and restore their sub-genres.
	  
	    int seperator = 0;
	    int startPoint = 0;
	    //ArrayList<String> unclassified = new ArrayList<String>();//String list for unclassified albums
	    for(int index = 0; index < allData.length(); index++){
		
			char nextChar = allData.charAt(index);//Gets the char at index.
			
			//Separate allData into genres and restore them.
			if(nextChar == Genre.delims.charAt(0))//if '#'
				seperator++;
			if(nextChar == Genre.delims.charAt(2))//if '%'
				seperator--;
			if(seperator == 0){
				if(nextChar == Genre.delims.charAt(1)){//if '@'
					index = allData.indexOf(';', startPoint);
					new Album(allData.substring(startPoint + 1, index)); //+1 to skip over @ character
				}
				else{
					String singleGenre = allData.substring(startPoint, index);
					Genre.restoreCollection(singleGenre);
				}
				startPoint = index + 1;
			}
	    }
	    
	    for(Genre g : Genre.genreMap.values())
	    	if(!music.contains(g) && !Genre.subGenres.contains(g.name))
	    		music.add(g);
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * @param fileName the file where to save the library
	 * 
	 * @throws RuntimeException if file cannot be opened
	 */
	public void saveCatalogueToFile(String fileName) throws RuntimeException {
		try{
			fileWriter = new PrintWriter(fileName);
		}
		catch (Exception e){
			throw new RuntimeException(e);
		}
		
		fileWriter.print(toString());
		fileWriter.close();
	}
	
	/**
	 * Helper function of save to file
	 * Note: Albums of an unclassied genre appear at the end of the string under the #unclassified tag
	 * 
	 * @return string representation of a catalogue
	 */
	public String toString(){
		String repres = "";
		
		for(Element e : music){
			repres += e.toString();
		}
		
		return repres;
	}
	
	
	
}