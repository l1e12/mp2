package ca.ubc.ece.eece210.mp2;


import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class to represent an entity in the catalogue. The element (in
 * this implementation) can either be an album or a genre.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public abstract class Element {
	protected ArrayList<Genre> genres; //branch to contain genres and sub genres
    protected ArrayList<Album> albums; //branch to contain the albums 
	
    /**
	 * Returns all the children of this entity. They can be albums or genres. In
	 * this particular application, only genres can have children. Therefore,
	 * this method will return the albums or genres contained in this genre.
	 * 
	 * @return the children
	 */
	//Assumption: The method is defined to only return the children of this particular level and 
	// not all of the lower levels that a genre may contain.
	public List<Element> getChildren() {
		List<Element> children = new ArrayList<Element>(); //will contain children of node

		if(hasChildren()){
			children.addAll(genres);
			children.addAll(albums);
		}
		
		return children;
	}

	/**
	 * Adds a child to this entity. Basically, it is adding an album or genre to
	 * an existing genre
	 * 
	 * @param b  
	 * 			the entity to be added.
	 * @throws IllegalStateException if the element cannot have children
	 */
	protected void addChild(Element b) throws IllegalStateException {
		if(hasChildren()){      //this element can have children (it is a genre)
			if(b.hasChildren()) //if b can have children, it is a genre
				genres.add((Genre)b);
			else                //if b cannot have children, it is an album
				albums.add((Album)b); 
			
		}
		else{                   //this element does not have children (it is an album)
			throw new IllegalStateException("Cannot add children to this element");
		}		
	}

	/**
	 * Abstract method to determine if a given entity can (or cannot) contain
	 * any children.
	 * 
	 * @return true if the entity can contain children, or false otherwise.
	 */
	public abstract boolean hasChildren();
	
}