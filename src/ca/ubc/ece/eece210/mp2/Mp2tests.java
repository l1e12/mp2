package ca.ubc.ece.eece210.mp2;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.junit.Test;

public class Mp2tests {

	@Test
	/**
	 * Test for the addition of an Album to Genre
	 * 1.)Album will get assigned a string representation of Genre it is in.
	 * 2.)Album cannot contain more than one genre.
	 * 3.)Album can be referenced by Genre. 
	 * 4.)Test adding an Album to a genre that does not exist.  
	 */
	public void Addtest() {
		String title = "DiscoTunez";
		String performer = "FunkMaster6";
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Boptastic");
		songlist.add("Dem Platform Shoez");
		songlist.add("Dont stop the Disco ball");
		
		Album DiscoTunez = new Album(title,performer,songlist);
		Genre disco = new Genre("Disco");
		
		DiscoTunez.addToGenre(disco);
		
		//Test that pointers are equal/ Album has Genre reference.
		assertEquals(disco, DiscoTunez.getGenre());
		
		//Test that album can be referenced by genre.
		assertEquals(disco.albums.get(0),DiscoTunez);
		
		//Test that the album cannot reference more than one genre.
		Genre disco2 = new Genre("Disco2");
		DiscoTunez.addToGenre(disco2);
		assertTrue(DiscoTunez.getGenre() != disco );
		
		//Resets static map
		Genre.genreMap.clear();
	}
	
	@Test
	/**
	 * Test for the removal of an Album from a genre
	 * 1.)Pointer to genre should disappear.
	 * 2.)Pointer to album should disappear.
	 */
	public void RemoveTest(){
	String title = "DiscoTunez";
		String performer = "FunkMaster6";
		ArrayList<String> songlist = new ArrayList<String>();
		songlist.add("Boptastic");
		songlist.add("Dem Platform Shoez");
		songlist.add("Dont stop the Disco ball");
		
		Album DiscoTunez = new Album(title,performer,songlist);
		Genre disco = new Genre("Disco");
		
		//Test Removing from a genre.
		DiscoTunez.addToGenre(disco);
		
		DiscoTunez.removeFromGenre();
		
		assertTrue(Genre.genreMap.get("unclassified")== DiscoTunez.getGenre());
		
		//Resets static map
		Genre.genreMap.clear();
	}
	
	@Test
	/**
	 * Test for the saving of an Album to string form.
	 * 1.) The string form should match the parsed sting form in
	 * the separation system we have made.
	 * Albums begin with title"//" performer"//"
	 * and all songs are separated by "//" and are followed by "$genre"
	 * 
	 * i.e. "title//performer//song1//song2//...//songN//$genre"
	 */
	public void SaveAlbumTest(){
		Genre g = new Genre("Rock");
		ArrayList<String> songList = new ArrayList<String>();
		
		//create album with title, performer, and no songs
		Album a = new Album("title", "performer", songList);
		
		assertEquals("title//performer//$unclassified", a.toString());
		
		a.addToGenre(g);
		
		assertEquals("title//performer//$Rock", a.toString());
		
		for(int x = 0; x < 3; x++)
			songList.add("song " + (x + 1));
		a = new Album("title", "performer", songList);
		
		assertEquals("title//performer//song 1//song 2//song 3//$unclassified", a.toString());
		
		//Resets static map
		Genre.genreMap.clear();
	}
	
	@Test
	/**
	 * Test to Recreate an Album from a string.
	 * 1.)Creating an album with a defined genre.
	 * 2.)Creating an album with no genre tag.
	 * 3.)Test to see that the Genre reference to album and 
	 * album reference to genre match.
	 */
	public void RecreateAlbumTest(){
		//undefined genre
		Album a = new Album("title//performer//song 1//song 2");
		
		assertTrue(a.getTitle().equals("title") && a.getPerformer().equals("performer") && a.getGenre().name.equals("unclassified"));
		for(int x = 0; x < a.getSongList().size(); x++)
			assertTrue(a.getSongList().get(x).equals("song " + (x+1)));
		
		//defined genre
		Genre g = new Genre("genre");
		a = new Album("title//performer//song 1//song 2//$genre");
		
		assertTrue(a.getTitle().equals("title") && a.getPerformer().equals("performer") && a.getGenre().name.equals("genre"));
		for(int x = 0; x < a.getSongList().size(); x++)
			assertTrue(a.getSongList().get(x).equals("song " + (x+1)));
		
		//genre references album, album references genre
		assertTrue(g.albums.get(0).equals(a) && a.getGenre().equals(g));
		
		//Resets static map
		Genre.genreMap.clear();	
	}
	@Test
	/**
	 * Test to save a Genre to a string form.
	 * Genres should be formatted to have Genres begin wtih "#",
	 * and be nested in side one another. Albums will begin with "@",
	 * have title, performer and songs separated by "//"'s and end with a
	 * reference to their genre proceed by a "$" and ending with a ";".
	 * The symbol "%" indicates the end of a genre.
	 */
	public void SaveGenreTest(){
		Genre rock = new Genre("rock");
		Genre alt = new Genre("alternative");
		new Album("Humbug//Arctic Monkeys//Crying Lightning//$alternative");
		new Album("Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$rock");
		
		rock.addToGenre(alt);
		
		assertEquals(rock.toString(), "#rock#alternative@Humbug//Arctic Monkeys//Crying Lightning//$alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$rock;%");
	
		//Resets static map
		Genre.genreMap.clear();
	}
	@Test
	/**
	 * Test to save the Catalogue to a sting
	 * A string produced from the file should match a test string
	 * that is created using the delimiter rules listed in the save genre test.
	 */
	public void CataloguetoFileTest(){
		Catalogue c = new Catalogue("testCatalogue.txt");
		c.saveCatalogueToFile("testOut.txt");
		
		File f = new File("testOut.txt");
		Scanner in = null;
		
		try {
			in = new Scanner(f);
		} catch (FileNotFoundException e) {
			assertNotNull(e);
		}
		//Output string that satisfies parameters of RecreateCatalogueTest:
		//  1) Subgenres are in super genres
		//  2) Unclassified albums are contained in unclassified genre
		String expectedPrint = "#Jazz#Blues@Blues Album//Armstrong//Song1//Song2//$Blues;%@Best Of//Chet Baker//My Funny Valentine//$Jazz;%#Rock#Punk#Grunge@Nevermind//Nirvana//Breed//$Grunge;%@American Idiot//Green Day//Whatsername//$Punk;%#Alternative@Humbug//Arctic Monkeys//Crying Lightning//$Alternative;@Indie 2//Artist//Songs!//$Alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$Rock;@Destroyer//Kiss//Detroit Rock City//$Rock;%#unclassified@Monstercat 008//Monstercat//Song//Song2//Song3//$unclassified;%#disco@Saturday Night Fever//John Travolta//Saturday Night Fever//$disco;%";
		
		if(in != null)
			assertEquals(expectedPrint, in.nextLine());
		
		//Resets static map
		Genre.genreMap.clear();
	}
	@Test
	/**
	 * Test to take a Catalogue file and recreate it.
	 * 1.)Test that sub-genres are contained in the super genre.
	 * 2.)Test that the unclassified albums are contained in unclassified.
	 */
	public void RecreateCatalougeTest(){
		//if toFile passes, the Catalogue was constructed correctly
		CataloguetoFileTest();
	}@Test
	/**
	 * Test to test the rules of Genres
	 * 1.)Sub-Genres can only belong to one genre.
	 * 2.)Genre contains a string representation of its name.
	 * 3.)An album in a sub-genre does not point to a genre above it.
	 */
	public void VerifyGenreTest(){
		//Will track the number of times a genre (name) is referenced as a sub genre
		ArrayList<Genre> subs = new ArrayList<Genre>();
		Catalogue c = new Catalogue("testCatalogue.txt");
		
		for(Genre g : c.music){
			traverseGenres(g, subs);
		}
		
		//Resets static map
		Genre.genreMap.clear();
	}
	/**
	 * Helper function to VerifyGenreTest
	 * Traverses genre tree to check cases of VerifyGenreTest
	 */
	public void traverseGenres(Genre g, ArrayList<Genre> subs){
		if(subs.contains(g)){
			fail("Genre occures twice");
			return;
		}
		if(g.name == null){
			fail("Genre does not contain a string name");
			return;
		}
		for(Album a : g.albums)
			if(a.getGenre() != g){
				fail("Album does not reference the genre above it");
				return;
			}
		for(Genre h : g.genres)
			traverseGenres(h, subs);
	}@Test
	/**
	 * Test to recreate a genre from a string.
	 * Using the delimiter rules stated in save Genre, test to see if 
	 * a Genre string representation will recreate a genre.
	 * 1.)Test that sub-genres are part of the super-genre
	 * 2.)Test that albums are contained in the proper genre/sub-genre.
	 * Note: the structure of our save files implies that (1) and (2) are true is the to strings are equivalent.
	 */
	public void RecreateGenreTest(){
		Genre rock = new Genre("rock");
		Genre alt = new Genre("alternative");
		new Album("Humbug//Arctic Monkeys//Crying Lightning//$alternative");
		new Album("Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$rock");
		String genre = "#rock#alternative@Humbug//Arctic Monkeys//Crying Lightning//$alternative;%@Don't Fear the Reaper//Blue Oyster Cult//Astronomy//$rock;%";
		rock.addToGenre(alt);
		
		Genre rock2 = Genre.restoreCollection(genre);
		
		assertEquals(rock.toString(),rock2.toString());
	    
		//Resets static map
		Genre.genreMap.clear();
	}

}
